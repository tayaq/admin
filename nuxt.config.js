export default {
    // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
    ssr: true,

    server: {
        port: 3030,
    },

    router: {
        linkActiveClass: 'is-active',
    },

    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'admin',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ],
        script: [
            {
                src: 'https://use.fontawesome.com/releases/v5.14.0/js/all.js'
            }
        ]
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [
        'quill/dist/quill.core.css',
        // for snow theme
        'quill/dist/quill.snow.css',
        // for bubble theme
        'quill/dist/quill.bubble.css'
    ],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [
        { src: '~plugins/nuxt-quill-plugin', ssr: false }
    ],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        '@nuxtjs/dotenv',
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/bootstrap
        '@nuxtjs/bulma',
        '@nuxtjs/dayjs',
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
    ],

    dayjs: {
        locales: ['ru'],
        defaultLocale: 'ru',
        defaultTimeZone: 'Europe/Kiev',
        plugins: [
            'timezone'
        ]
    },


    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {
        baseURL: process.env.BASE_URL
    },

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {
        publicPath: '/dist/',
        postcss: {
            preset: {
                features: {
                    customProperties: false
                }
            }
        },
        extend(config, ctx) {
            config.module.rules.push({
                test: /\.(ogg|mp3|wav|mpe?g)$/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            })
        }
    }
}

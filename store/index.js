export const state = () => ({
    categories: [],
    products: [],
    pages: [],
    orders: []
})

export const mutations = {
    setCategories(state, categories) {
        state.categories = categories;
    },
    setProducts(state, products) {
        state.products = products;
    },
    updateProductImages(state, image) {
        state.products.images.push(image);
    },
    setPages(state, pages) {
        state.pages = pages;
    },
    setOrders(state, orders) {
        state.orders = orders;
    }
}

export const actions = {

    async fetchCategories({ commit }) {
        const categories = await this.$axios.$get('categories');
        await commit('setCategories', categories);
    },

    async fetchPages({ commit }) {
        const pages = await this.$axios.$get('pages');
        await commit('setPages', pages);
    },

    async fetchProducts({ commit }) {
        const categories = await this.$axios.$get('products?list=all');
        await commit('setProducts', categories);
    },

    async fetchOrders({ commit }) {
        const orders = await this.$axios.$get('orders');
        await commit('setOrders', orders);
    },

    async nuxtServerInit({ dispatch }) {
        await dispatch('fetchCategories');
        await dispatch('fetchProducts');
        await dispatch('fetchPages');
        await dispatch('fetchOrders');
    }

}

export const getters = {
    categories: state => state.categories,
    products: state => state.products,
    pages: state => state.pages,
    orders: state => state.orders,
}
